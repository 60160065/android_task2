package buu.phatcharapol.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener{
            val intent = Intent(MainActivity@this, PlusActivity::class.java)
            startActivity(intent)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener{
            val intent = Intent(MainActivity@this, MinusActivity::class.java)
            startActivity(intent)
        }

        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener{
            val intent = Intent(MainActivity@this, MultiplyActivity::class.java)
            startActivity(intent)
        }
    }
}